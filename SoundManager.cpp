#include "SoundManager.h"

SoundManager::SoundManager()
{
}

void SoundManager::Play(Entity::Type type)
{
	size_t index = rand() % m_soundMap[type].size();
	Mix_PlayChannel(-1, m_soundMap[type][index], 0);
}

SoundManager* SoundManager::Get()
{
	static SoundManager s_instance;
	return &s_instance;
}

bool SoundManager::LoadSounds()
{
	static std::string goodSFX[] =
	{
		"Assets/SFX/Good0.wav",
		"Assets/SFX/Good1.wav",
		"Assets/SFX/Good2.wav",
		"Assets/SFX/Good3.wav",
	};

	static std::string badSFX[] =
	{
		"Assets/SFX/Bad0.wav",
		"Assets/SFX/Bad1.wav",
		"Assets/SFX/Bad2.wav",
		"Assets/SFX/Bad3.wav",
	};

	for (auto& file : goodSFX)
	{
		auto mapItr = m_soundMap.find(Entity::Type::Good);
		if (mapItr == m_soundMap.end())
			m_soundMap.emplace(Entity::Type::Good, std::vector<Mix_Chunk*>{ Mix_LoadWAV(file.c_str()) });
		else
			mapItr->second.emplace_back(Mix_LoadWAV(file.c_str()));
	}

	for (auto& file : badSFX)
	{
		auto mapItr = m_soundMap.find(Entity::Type::Evil);
		if (mapItr == m_soundMap.end())
			m_soundMap.emplace(Entity::Type::Evil, std::vector<Mix_Chunk*>{ Mix_LoadWAV(file.c_str()) });
		else
			mapItr->second.emplace_back(Mix_LoadWAV(file.c_str()));
	}

	m_pMusic = Mix_LoadMUS("Assets/Music/BGM.mp3");
	Mix_PlayMusic(m_pMusic, -1);
	return true;
}

void SoundManager::Cleanup()
{
	Mix_FreeMusic(m_pMusic);
	// TODO on return: Load sounds and play them
	// - Give points and take away points for good/bad nodes
	// - Programmer art for both
	for (auto& soundGroup : m_soundMap)
	{
		for (auto* pSound : soundGroup.second)
		{
			Mix_FreeChunk(pSound);
		}
	}
	m_soundMap.clear();
}
