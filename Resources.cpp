#include <GL/glew.h>

#include "Resources.h"

#define STB_IMAGE_IMPLEMENTATION
#include <STB/stb_image.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <iostream>

Resources::~Resources()
{
}

void Resources::Cleanup()
{
	for (auto& texItr : m_texIdMap)
	{
		glDeleteTextures(1, &texItr.second);
	}
	m_texIdMap.clear();

	for (auto& glyphItr : m_glyphMap)
	{
		glDeleteTextures(1, &glyphItr.second.texId);
	}
	m_glyphMap.clear();
}

bool Resources::CreateTextTextures(const char* fontFilepath, unsigned int size)
{
	FT_Library ft;
	if (FT_Init_FreeType(&ft))
		return false;

	FT_Face face;
	if (FT_New_Face(ft, fontFilepath, 0, &face))
		return false;

	FT_Set_Pixel_Sizes(face, 0, size);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);  // disable byte-alignment restrictions

	for (GLubyte c = 0; c < 128; ++c)
	{
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
			continue;

		GLuint texId;
		glGenTextures(1, &texId);
		glBindTexture(GL_TEXTURE_2D, texId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width,
			face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer);

		// Texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Store for later use
		Glyph glyph = { texId, glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			face->glyph->advance.x };
		m_glyphMap.emplace(c, glyph);
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ft);

	return true;
}

GLuint Resources::GetTexture(const char* filepath)
{
	auto idItr = m_texIdMap.find(filepath);
	if (idItr == m_texIdMap.end())
	{
		GLuint texId;
		glGenTextures(1, &texId);
		glBindTexture(GL_TEXTURE_2D, texId);

		// Texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		int width, height, nrChannels;
		stbi_set_flip_vertically_on_load(true);
		unsigned char* data = stbi_load(filepath, &width, &height, &nrChannels, 0);

		if (!data)
		{
			std::cerr << "Could not load texture: " << filepath << std::endl;
			return 0;
		}

		idItr = m_texIdMap.emplace(filepath, texId).first;

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		stbi_image_free(data);
	}
	
	return idItr->second;
}
