#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Rect.h"
#include "Entity.h"
#include "Shader.h"
#include "Resources.h"

class GameState;
struct GLFWwindow;

class Window
{
private:
	Rect m_workArea;
	Rect m_windowRect;

	GLFWwindow* m_pWindow;
	GameState* m_pGameState;

	// Static data that is memcpy'd to the member variables
	// I think that OpenGL requires different memory locations for each context
	static float s_vertices[20];
	static unsigned int s_indices[6];

	// Raw data for unit quad
	float m_vertices[20];
	unsigned int m_indices[6];

	// OpenGL objects
	GLuint m_VBO;
	GLuint m_VAO;
	GLuint m_EBO;
	Shader m_basicShader;

	// Text objects
	GLuint m_textVao;
	GLuint m_textVbo;
	Shader m_textShader;

	// HACK: Currently every window has it's own separate set of resources, which makes me feel sad
	// But time is of the essence!
	Resources m_resources;

	// Stops recursive looping when a player tries to move a window without any resources
	bool m_movedThisFrame;

	void CheckOGL();
	bool InitInternal(int x, int y, int width, int height, bool centerWindow);

public:
	// Minimum window sizes
	const int kMinWindowWidth = 100;
	const int kMinWindowHeight = 100;

	// This returns true if the center of the transform is inside the window.  Entity is not used currently
	bool EntityVisible(const Entity& entity, const glm::mat4& transform);

	bool InitCenetered(int width, int height);
	bool InitWithPosition(int x, int y, int width, int height);
	void QuitGame();
	void Render();
	void Cleanup();

	// NMC -> Normalized Monitor Coordinates  (0, 0) is the center of the monitor
	void RenderText(std::string text, GLfloat xNMC, GLfloat yNMC, GLfloat scale, glm::vec3 color);

	void SetWindowSize(int width, int height);
	void SetWindowPosition(int x, int y);
	void SetGameState(GameState* pGameState) { m_pGameState = pGameState; }
	void SetMovedThisFrame(bool hasMoved) { m_movedThisFrame = hasMoved; }

	GLFWwindow* GetGLFWwindow() { return m_pWindow; }
	const Rect& GetWorkArea() { return m_workArea; }
	Rect GetWindowRect() { return m_windowRect; }
	bool MovedThisFrame() { return m_movedThisFrame; }
	GameState* GetGameState() { return m_pGameState; }
};

void KeyCallback(GLFWwindow* pWindow, int key, int scancode, int action, int mods);
void ResizeWindowCallback(GLFWwindow* pWindow, int width, int height);
void MoveWindowCallback(GLFWwindow* pWindow, int x, int y);
void CloseWindowCallback(GLFWwindow* pWindow);

