#include "GL/glew.h"

#include <iostream>

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>


#include "Game.h"
#include "SoundManager.h"

bool Game::Init()
{
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
	{
		std::cout << SDL_GetError() << std::endl;
		return false;
	}

	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) < 0)
	{
		SDL_Quit();
		return false;
	}

	int mixFlags = MIX_INIT_MP3;
	if (Mix_Init(mixFlags) != mixFlags)
	{
		Mix_CloseAudio();
		SDL_Quit();
		return false;
	}

	SoundManager::Get()->LoadSounds();

	glewExperimental = true;
	if (!glfwInit())
	{
		return false;
	}

	CreateWindows(1);

	return true;
}

void Game::CreateWindows(int count)
{
	if (count == 5)
	{
		m_win = true;
		return;
	}

	// Cleanup previous windows
	for (size_t i = 0; i < m_pWindows.size(); ++i)
	{
		m_pWindows[i]->Cleanup();
		glfwDestroyWindow(m_pWindows[i]->GetGLFWwindow());
		delete m_pWindows[i];
	}
	m_pWindows.clear();

	// UBER HACK:  This is truly awful, but I only have 2 hours left
	std::unordered_map<int, std::vector<std::vector<glm::vec2>>> windowMap;
	windowMap[1].emplace_back(std::vector<glm::vec2>{ glm::vec2(0.0f, 0.0f)});
	windowMap[2].emplace_back(std::vector<glm::vec2>{ glm::vec2(100.0f, 300.0f), glm::vec2(1000.0f, 300.0f) });
	windowMap[2].emplace_back(std::vector<glm::vec2>{ glm::vec2(600.0f, 300.0f), glm::vec2(600.0f, 750.0f) });
	windowMap[2].emplace_back(std::vector<glm::vec2>{ glm::vec2(100.0f, 100.0f), glm::vec2(750.0f, 750.0f) });
	windowMap[2].emplace_back(std::vector<glm::vec2>{ glm::vec2(850.0f, 200.0f), glm::vec2(150.0f, 500.0f) });

	windowMap[3].emplace_back(std::vector<glm::vec2>{ glm::vec2(100.0f, 200.0f), glm::vec2(600.0f, 400.0f), glm::vec2(1000.0f, 600.0f) });
	windowMap[3].emplace_back(std::vector<glm::vec2>{ glm::vec2(1000.0f, 200.0f), glm::vec2(600.0f, 400.0f), glm::vec2(100.0f, 400.0f) });
	windowMap[3].emplace_back(std::vector<glm::vec2>{ glm::vec2(500.0f, 200.0f), glm::vec2(500.0f, 500.0f), glm::vec2(500.0f, 800.0f) });
	windowMap[3].emplace_back(std::vector<glm::vec2>{ glm::vec2(100.0f, 100.0f), glm::vec2(1000.0f, 100.0f), glm::vec2(500.0f, 650.0f) });

	windowMap[4].emplace_back(std::vector<glm::vec2>
		{ glm::vec2(100.0f, 100.0f), glm::vec2(1000.0f, 100.0f), glm::vec2(500.0f, 650.0f), glm::vec2(350.0f, 350.0f) });
	windowMap[4].emplace_back(std::vector<glm::vec2>
		{ glm::vec2(100.0f, 100.0f), glm::vec2(350.0f, 150.0f), glm::vec2(600.0f, 200.0f), glm::vec2(850.0f, 250.0f) });
	windowMap[4].emplace_back(std::vector<glm::vec2>
		{ glm::vec2(100.0f, 100.0f), glm::vec2(100.0f, 600.0f), glm::vec2(500.0f, 600.0f), glm::vec2(500.0f, 100.0f) });
	windowMap[4].emplace_back(std::vector<glm::vec2>
		{ glm::vec2(100.0f, 250.0f), glm::vec2(600.0f, 350.0f), glm::vec2(500.0f, 200.0f), glm::vec2(700.0f, 800.0f) });

	std::vector<glm::vec2> positions = windowMap[count][rand() % windowMap[count].size()];

	std::vector<glm::vec2> sizes =
	{
		{ 400, 350 },
		{ 350, 300 },
		{ 300, 250 },
		{ 250, 200 }
	};

	// EYYYYY.  Very awesome (in regards to getting text working)
	for (int i = 0; i < count; ++i)
	{
		Window* pWindow = new Window();

		//pWindow->InitCenetered(400, 200);
		if (count != 1)
		{
			pWindow->InitWithPosition((int)positions[i].x, (int)positions[i].y, (int)sizes[count - 1].x, (int)sizes[count - 1].y);
		}
		else
		{
			pWindow->InitCenetered((int)sizes[count - 1].x, (int)sizes[count - 1].y);
		}

		m_pWindows.emplace_back(pWindow);
	}

	m_gameState.Init(this);

	// Not sure if this is necessary anymore, but I don't have time to test it
	for (int i = 0; i < count; ++i)
	{
		m_pWindows[i]->SetGameState(&m_gameState);
	}
}

void Game::Run()
{
	while (!m_gameState.WantsQuit())
	{
		if (m_gameState.WantsNextLevel())
		{
			m_gameState.NextLevel();
		}

		for (size_t i = 0; i < m_pWindows.size(); ++i)
		{
			m_pWindows[i]->Render();
		}

		glfwPollEvents();
	}
}

void Game::Cleanup()
{
	SoundManager::Get()->Cleanup();
	Mix_CloseAudio();
	Mix_Quit();
	SDL_Quit();

	for (size_t i = 0; i < m_pWindows.size(); ++i)
	{
		glfwWindowShouldClose(m_pWindows[i]->GetGLFWwindow());
		delete m_pWindows[i];
	}
	m_pWindows.clear();
}

bool Game::TryToMove(int deltaX, int deltaY)
{
	if (m_resourceCount > 0 && (deltaX + deltaY) < m_resourceCount)
	{
		m_resourceCount -= (deltaX + deltaY);

		return true;
	}

	return false;
}

void Game::AddResources(int amount)
{
	m_resourceCount += amount;

	if (m_resourceCount < 0)
	{
		int dbg = 2;
		++dbg;

		// GAME OVER
	}
}

bool Game::EntityVisible(Entity* pEntity)
{
	for (auto& pWindow : m_pWindows)
	{
		// Use window position to transform entity
		glm::mat4 transform = glm::mat4(1.0f);

		// Entity calculates the transformations needed
		Rect windowRect = pWindow->GetWindowRect();
		Rect workArea = pWindow->GetWorkArea();

		glm::vec2 scale = pEntity->GetScale(windowRect);
		glm::vec2 translate = pEntity->GetTranslation(windowRect, workArea);

		transform = glm::translate(transform, glm::vec3(translate.x, translate.y, 0.0f));
		transform = glm::scale(transform, glm::vec3(scale.x, scale.y, 1.0f));
		
		if (pWindow->EntityVisible(*pEntity, transform))
		{
			return true;
		}
	}

	return false;
}
