#include <GL/glew.h>

#include "Shader.h"

#include <fstream>
#include <string>
#include <iostream>

Shader::~Shader()
{
	glDeleteProgram(m_programId);
}

void Shader::Use()
{ 
	glUseProgram(m_programId); 
}

bool Shader::Load(const char* vertShaderFilepath, const char* fragShaderFilepath)
{
	// Vertex shader
	m_vertId = glCreateShader(GL_VERTEX_SHADER);

	std::ifstream in(vertShaderFilepath);
	std::string source((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	in.close();

	const char* sourceStr = source.c_str();
	glShaderSource(m_vertId, 1, &sourceStr, nullptr);

	glCompileShader(m_vertId);
	GLint success;
	char infoLog[512] = {};
	glGetShaderiv(m_vertId, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(m_vertId, 512, nullptr, infoLog);
		std::cout << "Shader compilation failed: " << infoLog << std::endl;
		return false;
	}

	// Fragment shader
	m_fragId = glCreateShader(GL_FRAGMENT_SHADER);

	in.open(fragShaderFilepath);
	source = std::string((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	in.close();
	sourceStr = source.c_str();
	glShaderSource(m_fragId, 1, &sourceStr, nullptr);

	glCompileShader(m_fragId);
	glGetShaderiv(m_fragId, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(m_fragId, 512, nullptr, infoLog);
		std::cout << "Shader compilation failed: " << infoLog << std::endl;
		return false;
	}

	// Program
	m_programId = glCreateProgram();
	glAttachShader(m_programId, m_vertId);
	glAttachShader(m_programId, m_fragId);
	glLinkProgram(m_programId);

	glGetProgramiv(m_programId, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(m_programId, 512, nullptr, infoLog);
		std::cout << "Program linkage failed: " << infoLog << std::endl;
		return false;
	}

	glUseProgram(m_programId);

	glDeleteShader(m_vertId);
	glDeleteShader(m_fragId);

	return true;
}
