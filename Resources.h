#pragma once

#include <unordered_map>
#include <string>

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class Resources
{
public:
	struct Glyph
	{
		GLuint texId;
		glm::ivec2 size;
		glm::ivec2 bearing;
		GLuint advance;
	};
private:

	std::unordered_map<GLbyte, Glyph> m_glyphMap;
	std::unordered_map<std::string, GLuint> m_texIdMap;

public:

	~Resources();
	void Cleanup();
	bool CreateTextTextures(const char* fontFilepath, unsigned int size);
	GLuint GetTexture(const char* filepath);
	const Glyph& GetGlyph(GLbyte ch) { return m_glyphMap[ch]; }
};

