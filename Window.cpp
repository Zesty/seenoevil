#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Window.h"
#include "GameState.h"
#include "Game.h"
#include "SoundManager.h"

// TESTING: Multiple windows
#include <iostream>

unsigned int Window::s_indices[6] =
{
	0, 1, 3,
	1, 2, 3,
};

float Window::s_vertices[20] = {
	// Position				// Texture Coordinates
	.5f,  .5f, 0.0f,		1.0f, 1.0f,	// top right
	.5f, -.5f, 0.0f,		1.0f, 0.0f, // bottom right
   -.5f, -.5f, 0.0f,		0.0f, 0.0f, // bottom left
   -.5f,  .5f, 0.0f,		0.0f, 1.0f, // top left
};

bool Window::EntityVisible(const Entity& entity, const glm::mat4& transform)
{
	return transform[3][0] >= -1.0f && transform[3][0] <= 1.0f && transform[3][1] >= -1.0f && transform[3][1] <= 1.0f;
}

void Window::CheckOGL()
{
#if _DEBUG
	GLenum error = glGetError();  
	if (error) 
	{ 
		std::cout << glewGetErrorString(error) << std::endl; 
	}
#endif
}

bool Window::InitInternal(int x, int y, int width, int height, bool centerWindow)
{
	// Each OpenGL context needs it's own copy of my unit quad data
	memcpy_s(m_indices, 6 * sizeof(unsigned int), s_indices, 6 * sizeof(unsigned int));
	memcpy_s(m_vertices, 20 * sizeof(float), s_vertices, 20 * sizeof(float));

	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	//glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);

	m_pWindow = glfwCreateWindow(width, height, "--", nullptr, nullptr);
	if (!m_pWindow)
	{
		glfwTerminate();
		return false;
	}
	glfwMakeContextCurrent(m_pWindow);

	if (glewInit())
	{
		return false;
	}

	// Find primary monitor dimensions
	GLFWmonitor* pMonitor = glfwGetPrimaryMonitor();
	glfwGetMonitorWorkarea(pMonitor, &m_workArea.x, &m_workArea.y, &m_workArea.width, &m_workArea.height);

	// Store starting window data
	if (centerWindow)
	{
		m_windowRect.x = m_workArea.x + (m_workArea.width / 2) - (width / 2);
		m_windowRect.y = m_workArea.y + (m_workArea.height / 2) - (height / 2);
	}
	else
	{
		m_windowRect.x = x;
		m_windowRect.y = y;
	}

	m_windowRect.width = width;
	m_windowRect.height = height;

	// Center window
	glfwSetWindowPos(m_pWindow, m_windowRect.x, m_windowRect.y);

	glfwSetKeyCallback(m_pWindow, KeyCallback);
	glfwSetWindowSizeCallback(m_pWindow, ResizeWindowCallback);
	glfwSetWindowPosCallback(m_pWindow, MoveWindowCallback);
	glfwSetWindowCloseCallback(m_pWindow, CloseWindowCallback);
	glfwSetWindowUserPointer(m_pWindow, this);

	// Each OpenGL context needs it's own shader IDs
	m_basicShader.Load("basic.vert", "basic.frag");
	m_textShader.Load("text.vert", "text.frag");

	// Set up the current context data and bind
	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_EBO);
	glGenBuffers(1, &m_VBO);

	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(s_vertices), s_vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(s_indices), s_indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// Set up the text buffers, arrays, and attributes
	glGenVertexArrays(1, &m_textVao);
	glGenBuffers(1, &m_textVbo);
	glBindVertexArray(m_textVao);

	// Use DYNAMIC_DRAW because we will be changing the values very often
	glBindBuffer(GL_ARRAY_BUFFER, m_textVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, nullptr, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	
	// Unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Load textures for every single letter (expensive, considers this take N*memory, where
	// N is the number of windows active)
	m_resources.CreateTextTextures("Assets/Fonts/consola.ttf", 20);
	m_movedThisFrame = false;

	return true;
}

void Window::RenderText(std::string text, GLfloat xNMC, GLfloat yNMC, GLfloat scale, glm::vec3 color)
{
	m_textShader.Use();
	glUniform3f(glGetUniformLocation(m_textShader.GetId(), "textColor"), color.x, color.y, color.z);
	CheckOGL();

	// HACK: Removing features due to deadline
	// I think the solution is simple... Just convert the xpos to be in pixel space relative to the monitor
	// Keep in mind, bottom left is the origin of ortho, and top left is the origin on the monitor

	//glm::vec2 windowBottomLeft = glm::vec2(
	//	m_windowRect.x,
	//	m_windowRect.y + m_windowRect.height
	//);

	//GLfloat x = (m_workArea.width / 2.0f) - windowBottomLeft.x + (xNMC * (m_workArea.width / 2.0f));
	//GLfloat y =  -((m_workArea.height / 2.0f) - windowBottomLeft.y + (yNMC * (m_workArea.height / 2.0f)));

	GLfloat x = xNMC;
	GLfloat y = yNMC;

	// (0, 0) NMC should be (workArea.w / 2, workArea.h / 2)

	// Projection seems to be NDC coordinates (x and y at least)
	glm::mat4 projection = glm::ortho(0.0f, (float)m_windowRect.width, 0.0f, (float)m_windowRect.height);

	glUniformMatrix4fv(glGetUniformLocation(m_textShader.GetId(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	CheckOGL();
	glActiveTexture(GL_TEXTURE0);
	CheckOGL();
	glBindVertexArray(m_textVao);
	CheckOGL();

	// Walk through every character
	for (auto& ch : text)
	{
		Resources::Glyph glyph = m_resources.GetGlyph(ch);

		//GLfloat xpos = ndcPos.x;
		//GLfloat ypos = ndcPos.y;
		GLfloat xpos = x + glyph.bearing.x * scale;
		GLfloat ypos = y - (glyph.size.y - glyph.bearing.y) * scale;

		GLfloat w = glyph.size.x * scale;
		GLfloat h = glyph.size.y * scale;

		GLfloat vertices[6][4] = {
			{ xpos,		ypos + h,	0.0f, 0.0f },
			{ xpos,		ypos,		0.0f, 1.0f },
			{ xpos + w,	ypos,		1.0f, 1.0f },

			{ xpos,		ypos + h,	0.0f, 0.0f },
			{ xpos + w,	ypos,		1.0f, 1.0f },
			{ xpos + w,	ypos + h,	1.0f, 0.0f },
		};

		glBindTexture(GL_TEXTURE_2D, glyph.texId);
		CheckOGL();

		// Update VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, m_textVbo);
		CheckOGL();
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		CheckOGL();
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CheckOGL();

		glDrawArrays(GL_TRIANGLES, 0, 6);
		CheckOGL();

		x += (glyph.advance >> 6) * scale;
	}
	glBindVertexArray(0);
	CheckOGL();
	glBindTexture(GL_TEXTURE_2D, 0);
	CheckOGL();
}

bool Window::InitCenetered(int width, int height)
{
	return InitInternal(0, 0, width, height, true);
}

bool Window::InitWithPosition(int x, int y, int width, int height)
{
	return InitInternal(x, y, width, height, false);
}

void Window::QuitGame()
{
	m_pGameState->QuitGame();
}

void Window::Render()
{
	glfwMakeContextCurrent(m_pWindow);
	glUseProgram(m_basicShader.GetId());
	GLenum error = glGetError();

	// Enable transparency
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	for (auto& entity : m_pGameState->GetEntities())
	{
		// Use window position to transform entity
		glm::mat4 transform = glm::mat4(1.0f);

		// Entity calculates the transformations needed
		// Wew lad.  Took me hours to figure this out!  Super psyched!!
		glm::vec2 scale = entity->GetScale(m_windowRect);
		glm::vec2 translate = entity->GetTranslation(m_windowRect, m_workArea);

		transform = glm::translate(transform, glm::vec3(translate.x, translate.y, 0.0f));
		transform = glm::scale(transform, glm::vec3(scale.x, scale.y, 1.0f));

		if (EntityVisible(*entity, transform))
		{
			GLint tintLoc = glGetUniformLocation(m_basicShader.GetId(), "tint");
			CheckOGL();
			glUniform3fv(tintLoc, 1, glm::value_ptr(entity->GetTint()));
			CheckOGL();

			if (!entity->HasBestowedPoints())
			{
				SoundManager::Get()->Play(entity->GetType());
				m_pGameState->GetGame()->AddResources(entity->BestowePoints());

				if (entity->GetType() == Entity::Type::Good)
				{
					m_pGameState->CollectFlower();
					if (m_pGameState->CheckNextLevel())
					{
						//return;
					}
				}
			}
		}
		else
		{
			GLint tintLoc = glGetUniformLocation(m_basicShader.GetId(), "tint");
			glm::vec3 tint{ 1, 1, 1 };
			glUniform3fv(tintLoc, 1, glm::value_ptr(tint));
		}

		GLint transformLoc = glGetUniformLocation(m_basicShader.GetId(), "transform");
		glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(transform));
		CheckOGL();

		glBindTexture(GL_TEXTURE_2D, m_resources.GetTexture(Entity::s_filepathMap[entity->GetType()].c_str()));
		CheckOGL();

		glBindVertexArray(m_VAO);
		CheckOGL();

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		CheckOGL();
		glBindVertexArray(0);
		CheckOGL();
	}

	if (m_pGameState->GetGame()->HasWon() == false)
	{
		std::string UI = "Flowers: " + std::to_string(m_pGameState->GetFlowersCollected())
			+ "/" + std::to_string(m_pGameState->GetFlowersRequired());
		RenderText(UI, 0.0f, m_windowRect.height - 25, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));

		UI = "Points: " + std::to_string(m_pGameState->GetGame()->GetResources());
		RenderText(UI, 0.0f, 25.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
	}
	else
	{
		RenderText("You win!", 0.0f, 90.0f, 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		RenderText("Thank you for playing", 0.0f, 70.0f, 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		RenderText("by Billy Graban", 0.0f, 50.0f, 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));

	}

	glfwSwapBuffers(m_pWindow);
	CheckOGL();
	m_movedThisFrame = false;
}

void Window::Cleanup()
{
	glfwSetKeyCallback(m_pWindow, nullptr);
	glfwSetWindowSizeCallback(m_pWindow, nullptr);
	glfwSetWindowPosCallback(m_pWindow, nullptr);
	glfwSetWindowCloseCallback(m_pWindow, nullptr);

	m_resources.Cleanup();
}

void Window::SetWindowSize(int width, int height)
{
	m_windowRect.width = width;
	m_windowRect.height = height;
}

void Window::SetWindowPosition(int x, int y)
{
	m_windowRect.x = x;
	m_windowRect.y = y;
}

void ResizeWindowCallback(GLFWwindow* pWindow, int width, int height)
{
	Window* pMyWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
	if (pMyWindow->GetGameState()->WantsNextLevel())
	{
		glfwSetWindowPosCallback(pMyWindow->GetGLFWwindow(), nullptr);
		return;
	}
	Rect rect = pMyWindow->GetWindowRect();

	// Also check for minimum size requirements
	if (pMyWindow->MovedThisFrame() == false && width >= pMyWindow->kMinWindowWidth && height >= pMyWindow->kMinWindowHeight &&
		pMyWindow->GetGameState()->GetGame()->TryToMove(abs(width - rect.width), abs(height - rect.height)))
	{
		// Since I was not making the context current this resize was messing with other viewports!
		glfwMakeContextCurrent(pMyWindow->GetGLFWwindow());
		glViewport(0, 0, width, height);
		pMyWindow->SetWindowSize(width, height);
		pMyWindow->SetMovedThisFrame(true);
	}
	else if (pMyWindow->MovedThisFrame() == false)
	{
		// Since I was not making the context current this resize was messing with other viewports!
		glfwMakeContextCurrent(pMyWindow->GetGLFWwindow());
		glViewport(0, 0, rect.width, rect.height);
		glfwSetWindowSize(pMyWindow->GetGLFWwindow(), rect.width, rect.height);
	}

	pMyWindow->Render();
}

void KeyCallback(GLFWwindow* pWindow, int key, int scancode, int action, int mods)
{
	Window* pMyWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
	if (key == GLFW_KEY_ESCAPE)
	{
		pMyWindow->QuitGame();
	}
	else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
	{
		pMyWindow->GetGameState()->GetGame()->AddResources(100);
	}
}

void MoveWindowCallback(GLFWwindow* pWindow, int x, int y)
{
	Window* pMyWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
	if (pMyWindow->GetGameState()->WantsNextLevel())
	{
		glfwSetWindowPosCallback(pMyWindow->GetGLFWwindow(), nullptr);
		return;
	}
	Rect rect = pMyWindow->GetWindowRect();

	if (pMyWindow->GetGameState()->GetGame()->TryToMove(abs(rect.x - x), abs(rect.y - y)))
	{
		pMyWindow->SetWindowPosition(x, y);
	}
	else if(pMyWindow->MovedThisFrame() == false)
	{
		// I need to remember where the window was previously.  This is a bit of a problem, as calling
		// glfwSetWindowPosition will create a recursive loop.  I might need a tricky bool to make sure
		// I don't do this more than once per frame
		glfwSetWindowPos(pMyWindow->GetGLFWwindow(), rect.x, rect.y);
		pMyWindow->SetMovedThisFrame(true);
		//pMyWindow->SetWindowPosition(rect.x, rect.y);
	}

	pMyWindow->Render();
}

void CloseWindowCallback(GLFWwindow* pWindow)
{
	Window* pMyWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
	pMyWindow->QuitGame();
}
