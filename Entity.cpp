#include "Entity.h"

std::unordered_map<Entity::Type, std::string> Entity::s_filepathMap = 
{
	{ Entity::Type::Good, "Assets/Images/Good.png"},
	{ Entity::Type::Evil, "Assets/Images/Evil.png"},
	{ Entity::Type::Star, "Assets/Images/Star.png"},
};

bool Entity::Init(Type type, glm::vec2 position, glm::vec2 sizeInPixels)
{
	m_type = type;
	m_position = position;
	m_sizeInPixels = sizeInPixels;

	if (m_type == Type::Good)
	{
		m_tint = glm::vec3(0.0f, 0.4f + (rand() % 600) / 600.0f, (rand() % 1000) / 1000.0f);
		m_pointValue = 300;
	}
	else if(m_type == Type::Evil)
	{
		m_tint = glm::vec3(0.4f + (rand() % 600) / 600.0f, 0.0f, 0.0f);
		m_pointValue = -50;
	}
	else
	{
		// Stars don't give points
		m_pointsBestowed = true;
	}

	return true;
}

glm::vec2 Entity::GetScale(const Rect& window)
{
	float xScale = (m_sizeInPixels.x / 2.0f) / (window.width / 2.0f);
	float yScale = (m_sizeInPixels.y / 2.0f) / (window.height / 2.0f);

	return glm::vec2(xScale, yScale);
}

glm::vec2 Entity::GetTranslation(const Rect& window, const Rect& workArea)
{
	// Calculate the offset of an entity
	float xCenterNDC = ((float)window.x + ((float)window.width / 2.0f)) / ((float)workArea.width) * 2.0f - 1.0f;
	float yCenterNDC = ((float)window.y + ((float)window.height / 2.0f)) / ((float)workArea.height) * 2.0f - 1.0f;

	// Calculate the units in reference to the window
	float windowXRatio = (float)workArea.width / (float)window.width;
	float windowYRatio = (float)workArea.height / (float)window.height;

	return glm::vec2(-m_position.x * -windowXRatio + xCenterNDC * -windowXRatio,
		-m_position.y * windowYRatio + yCenterNDC * windowYRatio);
}

int Entity::BestowePoints()
{
	m_pointsBestowed = true;
	return m_pointValue;;
}
