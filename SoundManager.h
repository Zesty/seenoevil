#pragma once

#include <unordered_map>
#include <string>
#include <vector>

#include <SDL/SDL_mixer.h>

#include "Entity.h"

class SoundManager
{
private:
	SoundManager();
	std::unordered_map<Entity::Type, std::vector<Mix_Chunk*>> m_soundMap;
	Mix_Music* m_pMusic;
public:
	void Play(Entity::Type type);
	static SoundManager* Get();
	bool LoadSounds();
	void Cleanup();
};

