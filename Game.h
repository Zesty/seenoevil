#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Shader.h"
#include "Resources.h"
#include "Entity.h"
#include "Window.h"
#include "GameState.h"

constexpr int kStartWindowWidth = 300;
constexpr int kStartWindowHeight = 200;



class Game
{
private:
	// First test window
	Window m_window;
	bool m_win = false;

	std::vector<Window*> m_pWindows;

	// Game state shared across windows
	GameState m_gameState;
	int m_resourceCount = 1000;
public:

	bool Init();
	void CreateWindows(int count);
	void Run();
	void Cleanup();
	bool TryToMove(int deltaX, int deltaY);
	void AddResources(int amount);
	bool EntityVisible(Entity* pEntity);
	int GetResources() { return m_resourceCount; }
	bool HasWon() { return m_win; }
};
