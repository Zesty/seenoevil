#pragma once

#include <unordered_map>
#include <string>

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "Rect.h"

class Entity
{
public:
	enum class Type
	{
		Good,
		Evil,
		Star
	};

private:
	glm::vec2 m_position;
	Type m_type;
	glm::vec2 m_sizeInPixels;
	glm::vec3 m_tint;
	bool m_pointsBestowed;
	int m_pointValue;

public:

	static std::unordered_map<Type, std::string> s_filepathMap;

	Entity() = default;

	bool Init(Type type, glm::vec2 position, glm::vec2 sizeInPixels);

	void SetTint(glm::vec3 tint) { m_tint = tint; }

	Type GetType() { return m_type; }
	glm::vec2 GetScale(const Rect& window);
	glm::vec2 GetTranslation(const Rect& window, const Rect& workArea);
	glm::vec2 GetPosition() { return m_position; }
	glm::vec3 GetTint() { return m_tint; }
	bool HasBestowedPoints() { return m_pointsBestowed; }
	int BestowePoints();
};

