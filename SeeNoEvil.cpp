/*
- TODO:
	- Cheat check - Maximize
	- Don't let skulls spawn on start screens
	- Draw stars first always
	- Spinning stars
	- Disallow movement if the score goes below zero
	- Check memory leaks
	- Tutorial only shows on first play
	- Turn music back on
	- Don't use rand()!!!
	- Consider defining window creation in monitor NDC
	- Find a way to share resources between OpenGL contexts.  Current EVERY window has it's own separately
		loaded resources... *shudder*
	- Allow entities to be 'visible' on either centerVisible or completelyVisible
	- Shaded Text
	- Game Over
	- Credits
	- Error checking and logging
	- Play with node size and influencing score value
	- Fix defaultlib linker warning (Might require source hunting and recompiling)


- REQUIREMENTS:
	- Tutorial
		- Expand the window to the right to see more
		- Move the window down
		- Flower: 300 points
		- Check left
		- Skull: -50 points
		- Yellow stars will guide you
		- Red stars warn you
	- Game State (Levels, Progression, etc)
	- Full game loop
		- Loss
		- Win?
		- Replay

- DONE:
	- Collecting Resources
	- Use colored stars on a black background to direct users
	- Sound
	- Window Sizing/Moving resource
	- Text
	- Make text display in monitor NDC (Normalized Monitor Coordinates (NMC))
	- Multiple Windows
	- Sprites draw relative to monitor, not window
	- Load and draw many sprites
	- Tint sprites when visible
	- More assets (sound, images, music)
	- Implement the Close Button on the window, lol

- NOTES:
	- No time for an event system.  Close coupling is to be expected
*/
#include <ctime>

#include "Game.h"

int main()
{
	srand((unsigned int)time(nullptr));

	Game game;
	if (!game.Init())
	{
		return 1;
	}

	game.Run();
	game.Cleanup();

	return 0;
}