#version 330
out vec4 FragColor;

in vec2 texCoord;

uniform sampler2D ourTexture;
uniform vec3 tint;

void main()
{
	FragColor = texture(ourTexture, texCoord) * vec4(tint, 1);
}