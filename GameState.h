#pragma once

#include <vector>

#include "Entity.h"
#include "Resources.h"
#include "Shader.h"

class Game;

class GameState
{
protected:
	// All entities in game
	std::vector<Entity*> m_pEntities;
	Game* m_pGame;
	bool m_shouldQuit;
	bool m_wantsNextLevel;

	int m_flowersCollected = 0;
	int m_flowersToNextLevel = 3;
	void CreateEntities();

public:
	bool Init(Game* pGame);
	~GameState();

	void NextLevel();
	void QuitGame() { m_shouldQuit = true; }
	bool WantsQuit() { return m_shouldQuit; }
	bool WantsNextLevel() { return m_wantsNextLevel; }
	const std::vector<Entity*>& GetEntities() const { return m_pEntities; }
	Game* GetGame() { return m_pGame; }
	void CollectFlower() { ++m_flowersCollected; }
	int GetFlowersCollected() { return m_flowersCollected; }
	int GetFlowersRequired() { return m_flowersToNextLevel; }
	bool CheckNextLevel();
};

