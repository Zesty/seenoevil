#include <GL/glew.h>

#include "GameState.h"
#include "Game.h"

void GameState::NextLevel()
{
	for (auto& pEntity : m_pEntities)
	{
		delete pEntity;
	}

	m_pEntities.clear();

	m_pGame->CreateWindows((m_flowersToNextLevel / 3));
	CreateEntities();

	m_wantsNextLevel = false;
}

void GameState::CreateEntities()
{
	std::vector<Entity*> pNodes;
	for (int i = 0; i < m_flowersToNextLevel * 7; ++i)
	{
		Entity* pEntity = new Entity();

		do
		{
			float x = (float)((rand() % 2000) - 1000) / 1000.0f * 0.8f;
			float y = (float)((rand() % 2000) - 1000) / 1000.0f * 0.8f;
			int roll = rand() % 100;
			pEntity->Init(((roll < 50) ? Entity::Type::Good : Entity::Type::Evil),
				glm::vec2(x, y), glm::vec2(100, 100));
		} while (m_pGame->EntityVisible(pEntity));

		pNodes.emplace_back(pEntity);
	}

	for (int i = 0; i < (m_flowersToNextLevel / 2) * 75; ++i)
	{
		Entity* pEntity = new Entity();
		float x = (float)((rand() % 2000) - 1000.0f) / 1000.0f;
		float y = (float)((rand() % 2000) - 1000.0f) / 1000.0f;
		float size = 32 + (float)(rand() % 200) / 10.0f;
		pEntity->Init(Entity::Type::Star, glm::vec2(x, y), glm::vec2(size, size));
		m_pEntities.emplace_back(pEntity);

		// Find closest entity
		float closestDistanceSq = 1000000;
		Entity::Type closestType = Entity::Type::Good;
		for (size_t j = 0; j < pNodes.size(); ++j)
		{
			glm::vec2 enemyPos = pNodes[j]->GetPosition();
			float distanceSq = (enemyPos.x - x) * (enemyPos.x - x) + (enemyPos.y - y) * (enemyPos.y - y);
			if (distanceSq < closestDistanceSq)
			{
				closestDistanceSq = distanceSq;
				closestType = pNodes[j]->GetType();
			}
		}

		if (closestType == Entity::Type::Good)
		{
			pEntity->SetTint(glm::vec3(0.8f + float(rand() % 200) / 1000.0f, 0.8f + float(rand() % 200) / 1000.0f, 0));
		}
		else
		{
			pEntity->SetTint(glm::vec3(0.8f + float(rand() % 200) / 1000.0f, 0, 0));
		}
	}

	for (auto& pNode : pNodes)
	{
		m_pEntities.emplace_back(pNode);
	}
	pNodes.clear();
}

bool GameState::Init(Game* pGame)
{
	m_shouldQuit = false;
	m_pGame = pGame;
	m_wantsNextLevel = false;

	CreateEntities();

	return true;
}

GameState::~GameState()
{
	for (auto& pEntity : m_pEntities)
	{
		delete pEntity;
	}

	m_pEntities.clear();
}

bool GameState::CheckNextLevel()
{
	if (m_flowersCollected >= m_flowersToNextLevel)
	{
		++m_flowersToNextLevel;
		m_flowersCollected = 0;
		CreateEntities();
		m_wantsNextLevel = true;
		return true;
	}

	return false;
}
