#pragma once

#include <GLFW/glfw3.h>

class Shader
{
	GLuint m_vertId;
	GLuint m_fragId;
	GLuint m_programId;

public:
	~Shader();
	bool Load(const char* vertShaderFilepath, const char* fragShaderFilepath);
	void Use(); 

	GLuint GetId() { return m_programId; }
};

